package io.atom.maskitlog4j2;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.rewrite.RewritePolicy;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.SimpleMessage;
import org.apache.logging.log4j.status.StatusLogger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.atom.maskitlog4j2.PIIRewritePolicy.PII_REWRITE_POLICY;

/**
 * Personally identifiable information rewrite policy
 */
@Plugin(
    name = PII_REWRITE_POLICY,
    category = Core.CATEGORY_NAME,
    elementType = "rewritePolicy",
    printObject = true)
@SuppressWarnings("checkstyle:FinalClass")
public class PIIRewritePolicy implements RewritePolicy {
  protected static final Logger LOGGER = StatusLogger.getLogger();
  private static final int UNMASKED_CHARS_LENGTH = 4;
  private static final String MASK_CHARS = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";


  public static final String PII_REWRITE_POLICY = "PIIRewritePolicy";
  private PIIPatterns piiPatterns;

  private PIIRewritePolicy(PIIPatterns piiPatterns) {
    this.piiPatterns = piiPatterns;
  }

  @Override
  public LogEvent rewrite(LogEvent sourceLogEvent) {
    final Message sourceMessage = sourceLogEvent.getMessage();
    Message message = sourceLogEvent.getMessage();
    String maskedMessage = null;
    if (sourceMessage == null || !piiPatternDefined()) {
      return sourceLogEvent;
    }

    for (Pattern pattern: piiPatterns.regExpressions()) {
      Matcher matcher = pattern.matcher(sourceMessage.getFormattedMessage());
      while (matcher.find()) {
        if (maskedMessage == null) {
          maskedMessage = sourceMessage.getFormattedMessage();
        }

        maskedMessage = maskData(maskedMessage, matcher);
      }
    }

    if (maskedMessage != null) {
      message = new SimpleMessage(maskedMessage);
    }

    return new Log4jLogEvent
        .Builder(sourceLogEvent)
        .setMessage(message)
        .build();
  }

  private String maskData(String message, Matcher matcher) {
    StringBuilder part = new StringBuilder();
    String matchedPart = matcher.group();
    int maskLength = matchedPart.length() - UNMASKED_CHARS_LENGTH;
    maskLength = maskLength > UNMASKED_CHARS_LENGTH ? maskLength : matchedPart.length();
    part.append(MASK_CHARS.substring(0, maskLength))
        .append(matchedPart.substring(matchedPart.length() - (matchedPart.length() - maskLength)));
    return message.replaceAll(matchedPart, part.toString());
  }

  @PluginFactory
  public static PIIRewritePolicy createPolicy(@PluginAttribute(value =
      "piiPatternsImplClass", defaultString = "com.dna.maskitlog4j2.DefaultPIIPatternsImpl")
                                                final String piiPatternsImplClass) {
    PIIPatterns piiPatterns = null;
    try {
      Class<?> clazz = Class.forName(piiPatternsImplClass);
      piiPatterns = (PIIPatterns) clazz.newInstance();

    } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
      LOGGER.error("Failed to init piiPatternsImplClass for filter:", e);
    } catch (ClassCastException e) {
      LOGGER.error("piiPatternsImplClass provided should implement PIIPatterns:", e);
    }

    return new PIIRewritePolicy(piiPatterns);
  }

  private boolean piiPatternDefined() {
    return piiPatterns != null && piiPatterns.regExpressions() != null;
  }
}
