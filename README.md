# maskit-log4j2 : Mask sensitive data - log4j2

Personally identifiable information can be used to identity or locate a single person or to identify an individual in context. Although encryption techniques are available to protect PII information at rest and transit, there is large amounts of data being logged and it is important to mask sensitive data.

[![CircleCI](https://circleci.com/bb/smart-codemonkeys/maskit-log4j2/tree/master.svg?style=svg)](https://circleci.com/bb/smart-codemonkeys/maskit-log4j2/tree/master)
[![Total alerts](https://img.shields.io/lgtm/alerts/b/smart-codemonkeys/maskit-log4j2.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/smart-codemonkeys/maskit-log4j2/alerts/)
[![Language grade: Java](https://img.shields.io/lgtm/grade/java/b/smart-codemonkeys/maskit-log4j2.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/smart-codemonkeys/maskit-log4j2/context:java)
[![codecov](https://codecov.io/bb/smart-codemonkeys/maskit-log4j2/branch/master/graph/badge.svg)](https://codecov.io/bb/smart-codemonkeys/maskit-log4j2)