package io.atom.maskitlog4j2;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PIIRewritePolicyTest
 */
public class PIIRewritePolicyTest {
  private static final transient Logger LOGGER = LoggerFactory.getLogger(PIIRewritePolicyTest.class);
  private static final String SSN = "612080342";
  private static PIIRewritePolicy piiRewritePolicy = PIIRewritePolicy
      .createPolicy("io.atom.maskitlog4j2.DefaultPIIPatternsImpl");

  @Test
  public void testSSNWithDashesMasking() {
    LOGGER.info("this is test");
    LOGGER.info("SSN with dashes is {}, {}", SSN, SSN);
    LOGGER.info("this is test 1");
  }
}
