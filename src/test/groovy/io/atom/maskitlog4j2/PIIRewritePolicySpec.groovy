package io.atom.maskitlog4j2

import org.apache.logging.log4j.core.LogEvent
import org.apache.logging.log4j.core.impl.Log4jLogEvent
import org.apache.logging.log4j.message.SimpleMessage
import spock.lang.Specification
import spock.lang.Unroll

/**
 * PIIRewritePolicySpec
 */
class PIIRewritePolicySpec extends Specification {

    private static PIIRewritePolicy piiRewritePolicy = PIIRewritePolicy.createPolicy("io.atom.maskitlog4j2.DefaultPIIPatternsImpl")

    @Unroll
    "test mask for (#scenario)"() {
        given: "log event"
        LogEvent sourceLogEvent = logEvent(sourceMessage)

        when: "rewrite"
        LogEvent actualLogEvent = piiRewritePolicy.rewrite(sourceLogEvent)

        then: "assert actualLogEvent log message with excepted message"
        actualLogEvent.getMessage().getFormattedMessage() == expectedMessage

        where:
        scenario                            |                       sourceMessage                           |          expectedMessage
        "credit card masking"               | "Credit card number 4868960000533560"                         | "Credit card number XXXXXXXXXXXX3560"
        "credit card with spaces masking"   | "Credit card number with spaces 4868 9600 0053 3560"          | "Credit card number with spaces XXXXXXXXXXXXXXX3560"
        "not valid credit card number"      | "Credit card number is 1111111111111111"                      | "Credit card number is 1111111111111111"
        "visa masking"                      | "4111111111111111"                                            | "XXXXXXXXXXXX1111"
        "visa with dashes"                  | "4111-1111-1111-1111"                                         | "XXXXXXXXXXXXXXX1111"
        "master card masking"               | "5500000000000004"                                            | "XXXXXXXXXXXX0004"
        "master card with dashes"           | "5500-0000-0000-0004"                                         | "XXXXXXXXXXXXXXX0004"
        "american express masking"          | "340000000000009"                                             | "XXXXXXXXXXX0009"
        "discover card masking"             | "6011000000000004"                                            | "XXXXXXXXXXXX0004"
        "discover card with dashes"         | "6011-0000-0000-0004"                                         | "XXXXXXXXXXXXXXX0004"
        "ssn masking"                       | "612080342"                                                   | "XXXXX0342"
        "non ssn"                           | "1111111111111111"                                            | "1111111111111111"
        "ssn with dashes"                   | "612-08-0342"                                                 | "XXXXXXX0342"
        "ssn with spaces"                   | "612 08 0342"                                                 | "XXXXXXX0342"
    }

    def logEvent(String message) {
        return new Log4jLogEvent.Builder()
                .setMessage(new SimpleMessage(message)).build()
    }
}
