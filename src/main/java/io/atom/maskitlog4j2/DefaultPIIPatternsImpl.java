package io.atom.maskitlog4j2;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Default PII Patterns
 */
public class DefaultPIIPatternsImpl implements PIIPatterns {
  private static final Pattern CREDITCARD_PATTERN_GROUPED = Pattern.compile(
      "(?:4\\d{3}[ -]*\\d{4}[ -]*\\d{4}[ -]*\\d(?:\\d{3})?|5[1-5]\\d{2}[ -]"
          + "*\\d{4}[ -]*\\d{4}[ -]*\\d{4}|6(?:011|5[0-9]{2})[ -]*\\d{4}[ -]*\\d{4}[ -]"
          + "*\\d{4}|3[47]\\d{2}[ -]*\\d{6}[ -]*\\d{5}|3(?:0[0-5]|[68][0-9])\\d[ -]*\\d{6}"
          + "[ -]*\\d{4}|(?:2131|1800)[ -]*\\d{6}[ -]*\\d{5}|35\\d{2}[ -]*\\d{4}[ -]*\\d{4}[ -]*\\d{4})",
      Pattern.CASE_INSENSITIVE
          | Pattern.DOTALL | Pattern.MULTILINE);
  private static final Pattern CREDITCARD_PATTERN_BARE = Pattern.compile(
      "(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3"
          + "(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})",
      Pattern.CASE_INSENSITIVE
          | Pattern.DOTALL | Pattern.MULTILINE);
  private static final Pattern SSN_PATTERN = Pattern.compile("\\b\\d{3}[ -]?\\d{2}[ -]?\\d{4}\\b",
      Pattern.CASE_INSENSITIVE
          | Pattern.DOTALL | Pattern.MULTILINE);

  private static final Set<Pattern> REGEX_EXPRESSIONS = new HashSet<>();

  static {
    REGEX_EXPRESSIONS.add(CREDITCARD_PATTERN_GROUPED);
    REGEX_EXPRESSIONS.add(CREDITCARD_PATTERN_BARE);
    REGEX_EXPRESSIONS.add(SSN_PATTERN);
  }

  @Override
  public Set<Pattern> regExpressions() {
    return REGEX_EXPRESSIONS;
  }

  protected Set<Pattern> addPattern(Pattern pattern) {
    REGEX_EXPRESSIONS.add(pattern);

    return REGEX_EXPRESSIONS;
  }
}
