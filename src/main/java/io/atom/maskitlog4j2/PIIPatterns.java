package io.atom.maskitlog4j2;

import java.util.Set;
import java.util.regex.Pattern;

/**
 * PII Patterns
 */
public interface PIIPatterns {
  Set<Pattern> regExpressions();
}
